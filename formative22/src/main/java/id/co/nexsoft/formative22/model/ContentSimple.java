package id.co.nexsoft.formative22.model;

import javax.persistence.*;

@Entity
public class ContentSimple {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;
    private String title;
    @Column(columnDefinition = "TEXT")
    private String description;

    public ContentSimple(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public ContentSimple() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
