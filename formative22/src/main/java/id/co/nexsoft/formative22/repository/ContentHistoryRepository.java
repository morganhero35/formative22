package id.co.nexsoft.formative22.repository;

import id.co.nexsoft.formative22.model.ContentHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentHistoryRepository extends CrudRepository<ContentHistory, Integer> {

    ContentHistory findById(int id);

    List<ContentHistory> findAll();

    ContentHistory save(ContentHistory formCard);

    void delete(ContentHistory formCard);
}