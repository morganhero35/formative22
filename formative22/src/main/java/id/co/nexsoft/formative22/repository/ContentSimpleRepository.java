package id.co.nexsoft.formative22.repository;

import id.co.nexsoft.formative22.model.ContentSimple;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentSimpleRepository extends CrudRepository<ContentSimple, Integer> {

    ContentSimple findById(int id);

    List<ContentSimple> findAll();

    ContentSimple save(ContentSimple formCard);

    void delete(ContentSimple formCard);
}