package id.co.nexsoft.formative22.model;

import javax.persistence.*;

@Entity
public class ContentHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;
    @Column(columnDefinition = "TEXT")
    private String description;

    public ContentHistory(String description) {
        this.description = description;
    }

    public ContentHistory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
