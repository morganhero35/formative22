package id.co.nexsoft.formative22.controller;

import id.co.nexsoft.formative22.model.ContentHistory;
import id.co.nexsoft.formative22.model.ContentIcons;
import id.co.nexsoft.formative22.model.ContentSimple;
import id.co.nexsoft.formative22.model.Contents;
import id.co.nexsoft.formative22.service.ContentService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")
public class ContentController {

    ContentService service;

    public ContentController(ContentService service) {
        this.service = service;
    }

    @RequestMapping("/")
    public String hello() {
        return "index";
    }

    @PostMapping(path = "/addCard")
    @ResponseStatus(HttpStatus.CREATED)
    String addContent(Contents users, RedirectAttributes model) {
        service.saveCard(users);
        return "index";
    }

    @PostMapping(path = "/addIcon")
    @ResponseStatus(HttpStatus.CREATED)
    String addContentIcon(ContentIcons users, RedirectAttributes model) {
        service.saveIcon(users);
        return "index";
    }

    @PostMapping(path = "/addHistory")
    @ResponseStatus(HttpStatus.CREATED)
    String addContentHistory(ContentHistory users, RedirectAttributes model) {
        service.saveHistory(users);
        return "index";
    }

    @PostMapping(path = "/addSimple")
    @ResponseStatus(HttpStatus.CREATED)
    String addContentSimple(ContentSimple users, RedirectAttributes model) {
        service.saveSimple(users);
        return "index";
    }
}
