package id.co.nexsoft.formative22.repository;

import id.co.nexsoft.formative22.model.Contents;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentsRepository extends CrudRepository<Contents, Integer> {
    Contents findById(int id);

    List<Contents> findAll();

    Contents save(Contents formCard);

    void delete(Contents formCard);
}