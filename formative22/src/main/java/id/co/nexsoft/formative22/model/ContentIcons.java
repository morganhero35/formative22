package id.co.nexsoft.formative22.model;

import javax.persistence.*;

@Entity
public class ContentIcons {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;
    private String icon;
    private String title;
    @Column(columnDefinition = "TEXT")
    private String description;

    public ContentIcons(String icon, String title, String description) {
        this.icon = icon;
        this.title = title;
        this.description = description;
    }

    public ContentIcons() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
