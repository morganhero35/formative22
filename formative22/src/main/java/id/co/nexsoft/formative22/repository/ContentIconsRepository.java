package id.co.nexsoft.formative22.repository;

import id.co.nexsoft.formative22.model.ContentHistory;
import id.co.nexsoft.formative22.model.ContentIcons;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentIconsRepository extends CrudRepository<ContentIcons, Integer> {


    ContentIcons findById(int id);

    List<ContentIcons> findAll();

    ContentIcons save(ContentIcons formCard);

    void delete(ContentIcons formCard);
}