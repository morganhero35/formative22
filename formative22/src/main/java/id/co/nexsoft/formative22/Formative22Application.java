package id.co.nexsoft.formative22;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative22Application {

    public static void main(String[] args) {
        SpringApplication.run(Formative22Application.class, args);
    }

}
