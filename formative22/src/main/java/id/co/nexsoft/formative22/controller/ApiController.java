package id.co.nexsoft.formative22.controller;

import id.co.nexsoft.formative22.model.ContentHistory;
import id.co.nexsoft.formative22.model.ContentIcons;
import id.co.nexsoft.formative22.model.ContentSimple;
import id.co.nexsoft.formative22.model.Contents;
import id.co.nexsoft.formative22.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin()
public class ApiController {

    @Autowired
    ContentService service;

    @GetMapping(value = "/getAll")
    public List<Contents> getAllCard() {
        return service.getAllCard();
    }

    @GetMapping(value = "/getCard/{id}")
    public Contents getCardById(@PathVariable(value = "id") int id) {
        return service.getCardById(id);
    }

    @GetMapping(value = "/deleteCard/{id}")
    public void deleteCard(@PathVariable(value = "id") int id) {
        service.deleteCardById(id);
    }

    @GetMapping(value = "/getAllHistory")
    public List<ContentHistory> getAllHistory() {
        return service.getAllHistory();
    }

    @GetMapping(value = "/getAllSimple")
    public List<ContentSimple> getAllSimple() {
        return service.getAllSimple();
    }

    @GetMapping(value = "/getAllIcon")
    public List<ContentIcons> getAllIcon() {
        return service.getAllIcon();
    }


}
