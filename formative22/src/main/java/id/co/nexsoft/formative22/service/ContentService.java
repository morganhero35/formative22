package id.co.nexsoft.formative22.service;

import id.co.nexsoft.formative22.model.ContentHistory;
import id.co.nexsoft.formative22.model.ContentIcons;
import id.co.nexsoft.formative22.model.ContentSimple;
import id.co.nexsoft.formative22.model.Contents;
import id.co.nexsoft.formative22.repository.ContentHistoryRepository;
import id.co.nexsoft.formative22.repository.ContentIconsRepository;
import id.co.nexsoft.formative22.repository.ContentSimpleRepository;
import id.co.nexsoft.formative22.repository.ContentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContentService {
    @Autowired
    ContentsRepository contentsRepository;
    @Autowired
    ContentIconsRepository iconsRepository;
    @Autowired
    ContentHistoryRepository historyRepository;
    @Autowired
    ContentSimpleRepository simpleRepository;

    /*Contents Start*/
    public List<Contents> getAllCard() {
        return contentsRepository.findAll();
    }

    public Contents getCardById(int id) {
        return contentsRepository.findById(id);
    }

    public String saveCard(Contents contents) {
        this.contentsRepository.save(contents);
        return "Add Card Successfully !";
    }

    public String deleteCardById(int id) {
        Contents temp = contentsRepository.findById(id);
        contentsRepository.delete(temp);
        return "Delete Card Successfully !";
    }
    /*Content End*/


    /*Content Icon Start*/
    public String saveIcon(ContentIcons contentIcons) {
        this.iconsRepository.save(contentIcons);
        return "Add Content Icon Successfully!";
    }

    public List<ContentIcons> getAllIcon() {
        return iconsRepository.findAll();
    }

    public ContentIcons getIconById(int id) {
        return iconsRepository.findById(id);
    }

    public String deleteIconById(int id) {
        ContentIcons temp = iconsRepository.findById(id);
        iconsRepository.delete(temp);
        return "Delete Card Successfully !";
    }
    /*Content Icon End*/


    /*Content History Start*/
    public String saveHistory(ContentHistory contentHistory) {
        this.historyRepository.save(contentHistory);
        return "Add Content Icon Successfully!";
    }

    public List<ContentHistory> getAllHistory() {
        return historyRepository.findAll();
    }

    public ContentHistory getHistoryById(int id) {
        return historyRepository.findById(id);
    }

    public String deleteHistoryById(int id) {
        ContentHistory temp = historyRepository.findById(id);
        historyRepository.delete(temp);
        return "Delete Card Successfully !";
    }
    /*Content History End*/


    /*Content Simple Start*/
    public String saveSimple(ContentSimple contentSimple) {
        this.simpleRepository.save(contentSimple);
        return "Add Content Icon Successfully!";
    }

    public List<ContentSimple> getAllSimple() {
        return simpleRepository.findAll();
    }

    public ContentSimple getSimpleById(int id) {
        return simpleRepository.findById(id);
    }

    public String deleteSimpleById(int id) {
        ContentSimple temp = simpleRepository.findById(id);
        simpleRepository.delete(temp);
        return "Delete Card Successfully !";
    }
    /*Content History End*/


}
