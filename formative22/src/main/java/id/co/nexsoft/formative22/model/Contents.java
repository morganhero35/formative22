package id.co.nexsoft.formative22.model;

import javax.persistence.*;

@Entity
public class Contents {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;
    private String title;
    @Column(columnDefinition = "TEXT")
    private String content;

    public Contents(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public Contents() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
