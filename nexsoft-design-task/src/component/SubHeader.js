import React from 'react';

class SubHeader extends React.Component {
    render() {
        return (
            <div id="subheader">
                <div className="row">
                    <div className="twelve columns">
                        <p className="text-center">"Vision is the art of seeing what is
                            invisible to others" - Jonathan Swift</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default SubHeader;