
import React from 'react';
import axios from 'axios'

class ContentSimple extends React.Component {
	state = {
		simples: []
	}

	getSimple() {
		axios.get(`http://localhost:8080/getAllSimple`)
			.then(res => {
				const simples = res.data;
				this.setState({ simples });
				console.log(simples);
			})
	}
	componentDidMount() {
		this.getSimple();
	}

	render() {
		return (
			<div>
				<br></br>
				<dl class="tabs">{
					this.state.simples.map(simples =>
						<dd>
							<a href={"#simple" + simples.id}>{simples.title}</a>
						</dd>
					)}
				</dl>
				<ul class="tabs-content">
					{
						this.state.simples.map((simples, i) =>
							<li id={"simple" + simples.id + "Tab"}>
								<p>{simples.description}</p>
							</li>
						)
					}
				</ul>
			</div>
		)
	}
}

export default ContentSimple;