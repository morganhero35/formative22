import img1 from './../files/img/1.jpg';
import img2 from './../files/img/2.jpg';
import img3 from './../files/img/3.jpg';
import img4 from './../files/img/4.jpg';
import img5 from './../files/img/5.jpg';
import img6 from './../files/img/6.jpg';
import img7 from './../files/img/7.jpg';
import img8 from './../files/img/8.jpg';
import img9 from './../files/img/9.jpg';
import img10 from './../files/img/10.jpg';
import React from 'react';

class ImageSlider extends React.Component {
    render() {
        return (
            <div id="ei-slider" className="ei-slider">
                <ul className="ei-slider-large">
                    <li><img src={img1} className="responsiveslide" alt='' />
                        <div className="ei-title">
                            <h2>Dare to</h2>
                            <h3>Dream</h3>
                        </div></li>
                    <li><img src={img2} className="responsiveslide" alt='' />
                        <div className="ei-title">
                            <h2>Believe</h2>
                            <h3>in Yourself</h3>
                        </div></li>
                    <li><img src={img3} className="responsiveslide" alt='' />
                        <div className="ei-title">
                            <h2>Don't</h2>
                            <h3>Give Up</h3>
                        </div></li>
                    <li><img src={img4} className="responsiveslide" alt='' />
                        <div className="ei-title">
                            <h2>Just</h2>
                            <h3>Do It!</h3>
                        </div></li>
                    <li><img src={img5} className="responsiveslide" alt='' />
                        <div className="ei-title">
                            <h2>Never</h2>
                            <h3>Say Never</h3>
                        </div></li>
                    <li><img src={img6} className="responsiveslide" alt='' />
                        <div className="ei-title">
                            <h2>Love</h2>
                            <h3>Yourself</h3>
                        </div></li>
                    <li><img src={img7} className="responsiveslide" alt='' />
                        <div className="ei-title">
                            <h2>You're</h2>
                            <h3>Your Own Hero</h3>
                        </div></li>
                    <li><img src={img8} className="responsiveslide" alt='' />
                        <div className="ei-title">
                            <h2>Catch</h2>
                            <h3>Your Dream</h3>
                        </div></li>
                    <li><img src={img9} className="responsiveslide" alt='' />
                        <div className="ei-title">
                            <h2>Make It</h2>
                            <h3>Right</h3>
                        </div></li>
                    <li><img src={img10} className="responsiveslide" alt='' />
                        <div className="ei-title">
                            <h2>You're</h2>
                            <h3>Worth It</h3>
                        </div></li>
                </ul>
                <ul className="ei-slider-thumbs">
                    <li className="ei-slider-element">Current</li>
                    <li><a>Slide 1</a> <img src={img1}
                        className="slideshowthumb" alt='' /></li>
                    <li><a>Slide 2</a> <img src={img2}
                        className="responsiveslide" className="slideshowthumb" alt='' /></li>
                    <li><a>Slide 3</a> <img src={img3}
                        className="slideshowthumb" alt='' /></li>
                    <li><a>Slide 4</a> <img src={img4}
                        className="slideshowthumb" alt='' /></li>
                    <li><a>Slide 5</a> <img src={img5}
                        className="responsiveslide" className="slideshowthumb" alt='' /></li>
                    <li><a>Slide 6</a> <img src={img6}
                        className="slideshowthumb" alt='' /></li>
                    <li><a>Slide 7</a> <img src={img7}
                        className="slideshowthumb" alt='' /></li>
                    <li><a>Slide 8</a> <img src={img8}
                        className="responsiveslide" className="slideshowthumb" alt='' /></li>
                    <li><a>Slide 9</a> <img src={img9}
                        className="slideshowthumb" alt='' /></li>
                    <li><a>Slide 10</a> <img src={img10}
                        className="slideshowthumb" alt='' /></li>
                </ul>
            </div>
        )
    }
}

export default ImageSlider;