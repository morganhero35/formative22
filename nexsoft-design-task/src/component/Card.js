import React from 'react';
import axios from 'axios'

class Card extends React.Component {
    state = {
        cards: []
    }

    getCard() {
        axios.get(`http://localhost:8080/getAll`)
            .then(res => {
                const cards = res.data;
                this.setState({ cards });
                console.log(cards);
            })

    }

    componentDidMount() {
        this.getCard();
    }

    render() {
        return (
            this.state.cards.map(card =>
                <div className="four columns" id='{card.id}'>
                    <h5>{card.title}</h5>
                    <p>{card.content}</p>
                    <p>
                        <a href={card.button} className="readmore">Learn more</a>
                    </p>
                </div>
            )
        )
    }
}

export default Card;