import React from 'react';

class Footer extends React.Component {
    render() {
        return (
			<div id="footer">
				<footer className="row">
					<p className="back-top floatright">
						<a href="#top"><span></span></a>
					</p>
					<div className="four columns">
						<h1>ABOUT US</h1>
						Our goal is to keep clients satisfied!
					</div>
					<div className="four columns">
						<h1>GET SOCIAL</h1>
						<a className="social facebook" href=""></a> <a className="social twitter"
							href=""></a> <a className="social deviantart" href=""></a> <a
								className="social flickr" href=""></a> <a className="social dribbble"
									href=""></a>
					</div>
					<div className="four columns">
						<h1>NEWSLETTER</h1>
						<div className="row">
							<div className="ten columns">
								<input type="text" placeholder="Enter your e-mail address..." />
							</div>
							<div className="two columns">
								<a href="" className="button">GO</a>
							</div>
						</div>
					</div>
				</footer>
			</div>
        )
    }
}

export default Footer;