import React from 'react';
import axios from 'axios'

class ContentHistory extends React.Component {
    state = {
        history: []
    }

    getHistory() {
        axios.get(`http://localhost:8080/getAllHistory`)
            .then(res => {
                const history = res.data;
                this.setState({ history });
                console.log(history);
            })

    }

    componentDidMount() {
        this.getHistory();
    }

    render() {
        return (
            <div>
                {
                    this.state.history.map(history =>
                        <p>{history.description}</p>
                    )}
                <div class="panel">
                    <p>
                        <b>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                            do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                            enim ad minim veniam, quis nostrud exercitation ullamco laboris
                            nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum dolore eu fugiat
                            nulla pariatur.</b>
                    </p>
                    <p class="text-right">
                        <i>Ray Singer, Manager at "Nexsoft Design Task"</i>
                    </p>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <br />
            </div>
        )
    }
}

export default ContentHistory;