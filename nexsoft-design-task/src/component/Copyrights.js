import React from 'react';

class Copyrights extends React.Component {
    render() {
        return (
            <div className="copyright">
                <div className="row">
                    <div className="six columns">
                        &copy;<span> Copyright Your Agency Name</span>
                    </div>
                    <div className="six columns">
                        <span className="floatright">Design by <a href="">WowThemesNet</a>
                        </span>
                    </div>
                </div>
            </div>
        )
    }
}

export default Copyrights;