import React from 'react';
import axios from 'axios'

class Icon extends React.Component {
    state = {
        icons: []
    }

    getIcon() {
        axios.get(`http://localhost:8080/getAllIcon`)
            .then(res => {
                const icons = res.data;
                this.setState({ icons });
                console.log(icons);
            })

    }
    componentDidMount() {
        this.getIcon();
    }

    render() {
        return (
            <div>
                <div className="row">
                    <ul className="ca-menu">{
                        this.state.icons.map(icons =>
                            <li id={icons.id}><a href=""> <span className="ca-icon"><i class={icons.icon} aria-hidden="true"></i></span>
                                <div className="ca-content">
                                    <h2 className="ca-main">
                                        {icons.title}
                                    </h2>
                                    <h3 className="ca-sub">{icons.description}</h3>
                                </div>
                            </a></li>
                        )}
                    </ul>
                </div>
            </div>
        )
    }
}

export default Icon;