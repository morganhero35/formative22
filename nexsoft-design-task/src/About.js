import aboutimg1 from './files/img/team1.jpg';
import aboutimg2 from './files/img/team2.jpg';
import aboutimg3 from './files/img/team3.jpg';
import './About.css';
import Footer from './component/Footer.js'
import Copyrights from './component/Copyrights';

import ContentHistory from './component/ContentHistory.js'
import ContentSimple from './component/ContentSimple.js'

function About() {
	return (
		<div>
			<div className="row">
				<div className="eight columns">
					<div className="sectiontitle">
						<h4>Our History</h4>
					</div>
					<ContentHistory />
					<ContentSimple />
				</div>

				<div className="four columns">
					<div className="teamwrap">
						<img src={aboutimg1} width="300" height="200" alt='' />
					</div>
					<div className="teamwrap">
						<img src={aboutimg2} width="300" height="200" alt='' />
					</div>
					<div className="teamwrap">
						<img src={aboutimg3} width="300" height="200" alt='' />
					</div>
				</div>
			</div>
			<Footer />
			<Copyrights />
		</div>

	);
}

export default About;
