import './App.css';

import About from './About';
import Home from './Home';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link
} from "react-router-dom";

function App() {
	return (
		<Router>
			<div>
				<div className="row">
					<div className="headerlogo four columns">
						<div className="logo">
							<a>
								<h4>Nexsoft Design Task</h4>
							</a>
						</div>
					</div>
					<div className="headermenu eight columns">
						<nav>
							<ul className="nav-bar">
								<li><Link to="/">Home</Link></li>
								<li><a>Portofolio</a></li>
								<li><a>Blog</a></li>
								<li><a>Pages</a></li>
								<li><a>Features</a></li>
								<li><Link to="/about">About</Link></li>
							</ul>
						</nav>
					</div>
				</div>

				<Switch>
					<Route exact path="/">
						<Home />
					</Route>
					<Route path="/about">
						<About />
					</Route>
				</Switch>
			</div>
		</Router>

	);
}
export default App;
