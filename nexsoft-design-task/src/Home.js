import './App.css';
import ImageSlider from './component/ImageSlider.js'
import SubHeader from './component/SubHeader.js'
import CenterWhatWeDo from './component/CenterWhatWeDo.js'
import Footer from './component/Footer.js'
import Copyrights from './component/Copyrights';
import Testimonials from './component/Testimonials';
import Card from './component/Card.js';
import Icon from './component/Icon.js'

function Home() {
    return (
        <div>
            <ImageSlider />
            <SubHeader />
            <Icon />

            <div className="row">
                <CenterWhatWeDo />
                <Card />
            </div>

            <div className="row">
                <Testimonials />
            </div>
            <Footer />
            <Copyrights />
        </div>
    );
}
export default Home;
